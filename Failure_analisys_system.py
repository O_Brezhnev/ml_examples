
# coding: utf-8

# # This is part of failure analisys system in operation.

# In[1]:

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import MultipleLocator
get_ipython().magic('matplotlib')


# In[2]:

# upload dataset
main_df = pd.read_excel('New DF_en.xlsx')


# First of all, let's see how many failing systems we have. To do this, let's use 'quick and dirty' solution using python graph library Seaborn.

# In[3]:

import seaborn as sns
fail_freq=main_df[main_df['Type of failure'] != 'NOTCOM']

def plot_all_fails(df):
    count_fail = df.groupby('Failing system').count()
    sns.barplot(count_fail.index.values, 
                count_fail['Failing component'].values)
    plt.show()

plot_all_fails(fail_freq)


# Next let's see how many failures has one specific component.

# In[4]:

# Next we need dataset without not confirmed failures as main.
main_df = fail_freq


# At first, we need appropriate time to work with. Let's define unique values of time, and also unique failing systems and components (we need them later). And we should think also about timeline on plot (x axis).

# In[5]:

def add_time(df):
    df['Time'] = pd.PeriodIndex(year=main_df['Year'], 
           quarter=main_df['Quarter']).to_timestamp()
    return df

def unic(df): 
    time = df['Time'].unique()
    time.sort()
    devic = df['Failing component'].unique()
    devic.sort()
    compl = df['System no.'].unique()
    compl.sort()
    return time, devic, compl

def graph_dataset (x_axis):
    df = pd.DataFrame(index = x_axis)
    df['count'] = 0
    return df


# Don't forget about time bias for more appropriate visualisation.

# In[6]:

def change_time(x):
    cont = []
    for t in x.index:
        cont.append(t.replace(month = t.month+1, day = t.day+15))

    x.index = cont
    return x


# So we need to target specific component and highlight its failures in current dataset.

# In[7]:

def targeting (tg, df, cond):
    tg_df = df[df['Failing component'] == tg]
    grp = tg_df.groupby([cond]).count()
    return tg_df, grp


# Now we should create data for y axis.

# In[8]:

def construct_plot_df(grp, plot_df):
    for j in grp.index:
        plot_df.loc[j]['count'] = grp.loc[j]['Failing component']
        
    return plot_df


# And plotting the graph.

# In[9]:

def plot_time (df, tg_df):
    bar_width = 80
    change_time(df)
    plt.bar(df.index, df['count'], width=bar_width, 
            color = ['g','c','y'])
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%b/%Y'))
    plt.gca().yaxis.set_minor_locator(MultipleLocator(2))
    plt.gca().yaxis.set_major_locator(MultipleLocator(1))
    plt.xlabel('Operating time')
    plt.ylabel('Number of failures')
    plt.title('%s %s: Number of failures in operation'% 
              (tg_df['Type of failing component'].iloc[0], 
               tg_df['Failing component'].iloc[0]))
    plt.show()


# Finally let's launch our code.

# In[11]:

main_df = add_time(main_df)
target = 'WEA-15Y'
timeline, devices, systems = unic(main_df)
target_df, work_group = targeting(target, main_df, 'Time')
time_df = graph_dataset (timeline)
construct_plot_df(work_group, time_df)
plot_time(time_df, target_df)


# Next we need to look at failures of this target component in different systems.

# In[12]:

def plot_sys (df, tg_df):
    bar_width = 0.3
    plt.bar(df.index, df['count'], width=bar_width, color = ['g','c','y'])
    plt.gca().xaxis.set_minor_locator(MultipleLocator(2))
    plt.gca().xaxis.set_major_locator(MultipleLocator(1))
    plt.gca().yaxis.set_minor_locator(MultipleLocator(2))
    plt.gca().yaxis.set_major_locator(MultipleLocator(1))
    plt.xlabel('System (number)')
    plt.ylabel('Number of failures')
    plt.title('%s %s: Number of failures in different systems'% 
              (tg_df['Type of failing component'].iloc[0], 
               tg_df['Failing component'].iloc[0]))
    plt.show()


# Let's plot this graph.

# In[13]:

sys_df = graph_dataset (systems)
target_df, work_group = targeting(target, main_df, 'System no.')
construct_plot_df(work_group, sys_df)
plot_sys(sys_df, target_df)


# Let's try to use machine learning to predict failures of specific devices in particular systems. First of all, we should create dataset for learning. This dataset should have failures as much as happenings of normal work. Also we need to change device unique names for correct work of our algorithm.

# In[14]:

def df_for_ml (df, compl, devic):
    ml_df = pd.DataFrame()
    for i in compl:
        for j in devic:
            row = df[(df['Failing component'] == j) & 
                          (df['System no.'] == i)].count()['Year']
            ml_df = ml_df.append([[i,j,row]])
            
    return ml_df

def replace_ids(df, devic):
    k = 0
    for i in devic:
        df[1].replace(i,k, inplace = True)
        k+=1


# And divide this new dataset into feature and target parts.

# In[15]:

def ml_datasets (df):   
    X = df[[0,1]]
    T = df[[2]]
    return X,T


# Let's use Decision Tree Classifier to train our model. We use cross validation to divide our dataset to train and test splits and estimate our accuracy. And we should don't forget to set max depth of our decision tree to prevent overfitting.

# In[16]:

def ml_train(X,T):
    from sklearn.model_selection import train_test_split
    from sklearn.tree import DecisionTreeClassifier
    X_train, X_test, y_train, y_test = train_test_split(X, T, random_state = 3)
    clf = DecisionTreeClassifier(max_depth = 6)
    clf.fit(X_train, y_train)
    print('Accuracy of Decision Tree classifier on training set: {:.2f}'
          .format(clf.score(X_train, y_train)))
    print('Accuracy of Decision Tree classifier on test set: {:.2f}'
          .format(clf.score(X_test, y_test)))
    return clf


# And finally we launch our code.

# In[18]:

ml_df = df_for_ml(main_df, systems, devices)  
replace_ids(ml_df, devices)
X,T = ml_datasets(ml_df)
clf = ml_train(X,T)


# In[ ]:



